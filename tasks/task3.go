package main

import (
	"fmt"
)

func main() {
	var r float32 = 10.04

	// fmt.Scanf("%f", &r)
	fmt.Println("R =", r)

	fmt.Printf("Area: %0.2f\n", area(r))
}

func area(r float32) (area float32) {
	const pi float32 = 3.14159

	areaOfCircle := pi * (r * r)
	areaOfSquare := (2 * r) * (2 * r)
	area = areaOfSquare - areaOfCircle
	return area
}
